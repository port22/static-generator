---
title: "Security Certifications"
description: 
tags: []
images: []
audio: []
video: []
series: []
date: 2018-11-14T14:42:47Z

draft: true
---

This post will discus a number of security standards which are used to verify desktops, servers, and applications.

## FIPS 140-2

Also, check out NIST CSF

- https://en.wikipedia.org/wiki/FIPS_140-2
- https://www.nist.gov/cyberframework 

## Common Criteria ISO/IEC 15408

Common Criteria (CC) for Information Technology Security Evaluation

- https://en.wikipedia.org/wiki/Common_Criteria
- https://www.commoncriteriaportal.org

## STIG

Security Technical Implementation Guides (STIG) 

- <https://en.wikipedia.org/wiki/Security_Technical_Implementation_Guide> 

## CIS Benchmark

Centre for Internet Security (CIS) is a non-profit organization

- <https://en.wikipedia.org/wiki/Center_for_Internet_Security>

# Misc

Ubuntu's review: 

- <https://blog.ubuntu.com/2018/09/26/canonicals-security-certifications>
